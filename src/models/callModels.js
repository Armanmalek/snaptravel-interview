import Immutable from 'immutable';

const callModel = Immutable.Record({
	callSuccess: null,
});

export const callInitialState = callModel({
	callSuccess: false,
});
