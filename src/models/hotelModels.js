import Immutable from 'immutable';

export const HotelModel = Immutable.Record({
	id: null,
	hotelName: null,
	reviews: null,
	address: null,
	rating: null,
	amenities: Immutable.List(),
	imageUrl: null,
	price: null,
	retailPrice: null,
	savings: null,
});

export const InitialHotelsModel = Immutable.List();
