import React, { Component } from 'react';
import SnapTravelLayout from 'components/SnapTravelLayout.jsx';
import SearchPage from 'components/SearchPage.jsx';
import ResultsPage from 'components/ResultsPage.jsx';
import './App.css';

class App extends Component {
	render() {
		return (
			<SnapTravelLayout>
				<SearchPage />
				<ResultsPage />
			</SnapTravelLayout>
		);
	}
}

export default App;
