export const hotelPayloaders = ({
	id,
	hotel_name,
	num_reviews,
	address,
	num_stars,
	amenities,
	image_url,
	price,
}) => ({
	id,
	hotelName: hotel_name,
	reviews: num_reviews,
	rating: num_stars,
	address,
	amenities,
	imageUrl: image_url,
	price,
});
