import React from 'react'
import StarRatings from 'react-star-ratings';
import PropTypes from 'prop-types';
import HotelEntryDetailsAmenities from './HotelEntryDetailsAmenities.jsx';
import './HotelEntryDetails.css';

const rootClass = 'hotel-entry-details';
const imageClass = `${rootClass}__image`;
const infoClass = `${rootClass}__info`;
const attributesClass = `${infoClass}__attributes`;

const HotelEntryDetails = ({ imageUrl, hotelName, rating, amenities =  null, reviews, savings }) => {
	return (
		<div className={rootClass}>
			<div className={imageClass}>
				<img
					src={imageUrl}
					alt="Shot of Hotel Exterior"
				/>
				{
					savings > 0
					? <span className={`${imageClass}__savings`}>{`$${savings} off!`}</span>
					: null
				}
			</div>
			<div className={infoClass}>
				<div className={`${infoClass}__title`}>
					{hotelName}
				</div>
				<div className={attributesClass}>
					<div className={`${attributesClass}__ratings-reviews`}>
						<StarRatings
							rating={rating}
							starRatedColor="gold"
							numberOfStars={5}
							name='rating'
							starDimension="30px"
						/>
						<div className={`${attributesClass}__ratings-reviews__reviews`}>
							{`${reviews} reviews`}
						</div>
					</div>
					<HotelEntryDetailsAmenities
						amenities={amenities}
					/>
				</div>
			</div>
		</div>
	);
};

HotelEntryDetails.propTypes = {
	imageUrl: PropTypes.string.isRequired,
	hotelName: PropTypes.string.isRequired,
	rating: PropTypes.number.isRequired,
	amenities: PropTypes.array,
	reviews: PropTypes.number.isRequired,
	savings: PropTypes.number.isRequired,
};

export default HotelEntryDetails;

