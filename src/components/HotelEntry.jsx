import React from 'react'
import ImmutablePropTypes from 'react-immutable-proptypes';
import HotelEntryPriceBoxes from './HotelEntryPriceBoxes';
import HotelEntryDetails from './HotelEntryDetails';
import './HotelEntry.css';

const rootClass = 'hotel-entry';

const HotelEntry = ({ hotel }) => {
	const {
		retailPrice,
		price,
		imageUrl,
		hotelName,
		rating,
		amenities,
		reviews,
		savings,
	} = hotel;

	return (
		<div className={rootClass}>
			<HotelEntryDetails
				imageUrl={imageUrl}
				hotelName={hotelName}
				rating={rating}
				amenities={amenities}
				reviews={reviews}
				savings={savings}
			/>
			<HotelEntryPriceBoxes
				retailPrice={retailPrice}
				price={price}
			/>
		</div>
	);
};

HotelEntry.propTypes = {
	hotel: ImmutablePropTypes.list.isRequired,
};

export default HotelEntry;
