import React from 'react';
import PropTypes from 'prop-types';
import './HotelEntryDetailsAmenities.css';

const rootClass = 'hotel-entry-details-amenities';

const HotelEntryDetailsAmenities = ({ amenities = null }) => {
	if (!amenities) return null;

	return (
		<div className={rootClass}>
			{
				amenities.map(amenity => (
					<span
						className={`${rootClass}__amenity`}
						key={amenity}
					>
						{amenity}
					</span>
				))
			}
		</div>
	);
};

HotelEntryDetailsAmenities.propTypes = {
	amenities: PropTypes.array,
};

export default HotelEntryDetailsAmenities;
