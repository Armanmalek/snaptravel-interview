import React from 'react';
import PropTypes from 'prop-types';
import FilterEntry from './FilterEntry.jsx';
import './FilterEntry.css';

const filterKeywords = ['Price', 'Rating', 'Savings'];
const rootclass = 'filter-bar';

const FilterBar = ({ onClick, currentlySortedFilter }) => {
	return (
		<div className={rootclass}>
			{
				filterKeywords.map(keyword => (
					<FilterEntry
						keyword={keyword}
						onClick={(s) => onClick(s)}
						active={currentlySortedFilter === keyword ? 'active' : null}
						key={keyword}
					/>
				))
			}
		</div>
	);
};

FilterBar.propTypes = {
	onClick: PropTypes.func.isRequired,
	currentlySortedFilter: PropTypes.string.isRequired,
};

export default FilterBar;
