import React from 'react';
import PropTypes from 'prop-types';
import { modifyClassName } from 'utils/classNameUtils.js';
import './SearchField.css';

const rootClass = 'search-field';

const SearchField = ({ placeholder, classModifier, type, onChange, id }) => {
	return (
		<input
			className={modifyClassName(rootClass, classModifier)}
			type={type}
			id={id}
			placeholder={placeholder}
			onChange={onChange}
		/>
	);
};

SearchField.defaultProps = {
	classModifier: null,
	type: 'string',
};

SearchField.propTypes = {
	classModifier: PropTypes.string,
	pattern: PropTypes.string,
	placeholder: PropTypes.string.isRequired,
	type: PropTypes.string,
	onChange: PropTypes.func,
};

export default SearchField;
