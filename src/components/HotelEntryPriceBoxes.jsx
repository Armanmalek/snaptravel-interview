import React from 'react';
import PropTypes from 'prop-types';
import { modifyClassName } from 'utils/classNameUtils.js';
import './HotelEntryPriceBoxes.css';

const rootClass = 'hotel-entry-price-boxes';
const retailClass = `${rootClass}__retail`;
const snapClass = `${rootClass}__snap`;

const HotelEntryPriceBoxes = ({ retailPrice, price }) => {
	const samePriceModifier = retailPrice === price ? 'same-price' : null;
	return (
		<div className={rootClass}>
			<div className={retailClass}>
				<span className={modifyClassName(`${retailClass}__price`, samePriceModifier)}>
					{`USD$${retailPrice}`}
				</span>
				{'Their Price'}
			</div>
			<div className={snapClass}>
				<span>
					{`USD$${price}`}
				</span>
				{'Our Price!'}
			</div>
		</div >
	);
};

HotelEntryPriceBoxes.propTypes = {
	retailPrice: PropTypes.number.isRequired,
	price: PropTypes.number.isRequired,
};

export default HotelEntryPriceBoxes;
