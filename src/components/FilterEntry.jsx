import React from 'react';
import PropTypes from 'prop-types';
import { modifyClassName } from 'utils/classNameUtils.js';
import './FilterEntry.css';

const rootClass = 'filter-entry';
const FilterEntry = ({ active, onClick, keyword }) => {
	return (
		<div
			className={modifyClassName(rootClass, active)}
			onClick={() => onClick(keyword)}
		>
			{keyword}
			{active
				? <img
					src={process.env.PUBLIC_URL + '/images/filter-icon.png'}
					className={`${rootClass}__image`}
					alt="filter icon"
				/>
				: null
			}
		</div>
	);
};

FilterEntry.propTypes = {
	active: PropTypes.string.isRequired,
	onClick: PropTypes.func.isRequired,
	keyword: PropTypes.string.isRequired,
};

export default FilterEntry;
