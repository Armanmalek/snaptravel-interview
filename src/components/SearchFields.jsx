import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import { Formik } from 'formik';
import SearchField from './SearchField.jsx';
import { callHotels } from '../services/hotelService.js';
import './SearchFields.css';

const rootClass = 'search-fields';
const submitClass = `${rootClass}__submit`;


class SearchFields extends React.Component {
	static propTypes = {
		callHotels: PropTypes.func.isRequired,
	}

	constructor(props) {
		super(props);

		this.state = {
			city: null,
			checkIn: null,
			checkOut: null,
		};
	}

	handleSubmission(values) {
		const {
			callHotels,
		} = this.props;

		callHotels(values);
	}

	render() {
		return (
			<Formik
				initialValues={{
					city: "",
					checkIn: "",
					checkOut: "",
				}}
				onSubmit={(values, { setSubmitting }) => {
					setSubmitting(false);
					this.handleSubmission(values);

				}}
			>
				{props => {
					const {
						values,
						isSubmitting,
						handleChange,
						handleBlur,
						handleSubmit,
					} = props;
					return (
						<form
							className={rootClass}
							onSubmit={handleSubmit}
						>
							<SearchField
								id="city"
								placeholder="Search By City"
								classModifier='search'
								value={values.city}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<SearchField
								id="checkIn"
								placeholder="Check-In"
								classModifier='date'
								isDateField={true}
								value={values.checkIn}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<SearchField
								id="checkOut"
								placeholder="Check-Out"
								classModifier='date'
								isDateField={true}
								value={values.checkOut}
								onChange={handleChange}
								onBlur={handleBlur}
							/>
							<button className={submitClass} type="submit" disabled={isSubmitting}>
								<img
									className={`${submitClass}__image`}
									src={process.env.PUBLIC_URL + '/images/search-icon.png'}
									alt="submit"
								/>
							</button>
						</form>
					)
				}}
			</Formik >
		)
	}
}

const mapDispatchToProps = (dispatch) => ({
	callHotels: bindActionCreators(callHotels, dispatch)
});

export default connect(null, mapDispatchToProps)(SearchFields);
