import React from 'react';
import PropTypes from 'prop-types';
import ImmutablePropTypes from 'react-immutable-proptypes';
import { connect } from 'react-redux';
import { bindActionCreators } from 'redux';
import FilterBar from './FilterBar.jsx';
import HotelEntry from './HotelEntry.jsx';
import { sortHotels } from 'actions/hotelActions.js';

const rootClass = 'results-page';

class ResultsPage extends React.Component {
	static propTypes = {
		searchComplete: PropTypes.bool.isRequired,
		hotels: ImmutablePropTypes.list,
		sortHotels: PropTypes.func.isRequired,
	};

	static defaultProps ={
		hotels: null,
	};

	constructor(props) {
		super(props);

		this.state = {
			currentlySortedFilter: 'Price',
		};
	}

	getSortingAttribute(sortingType) {
		switch (sortingType) {
			case 'Savings': {
				return 'savings';
			}
			case 'Price': {
				return 'price';
			}
			case 'Rating': {
				return 'rating';
			}
			default: {
				return sortingType;
			}
		}
	}

	handleFilterChange(sortingType) {
		const {
			sortHotels
		} = this.props;

		const sortingAttribute = this.getSortingAttribute(sortingType);

		if (sortingAttribute === this.state.currentlySortedFilter) return;

		this.setState({
			currentlySortedFilter: sortingAttribute
		},
			() => sortHotels(sortingAttribute)
		);
	}

	render() {
		const {
			searchComplete,
			hotels,
		} = this.props;

		if (!searchComplete) return null;

		return (
			<div className={rootClass}>
				<FilterBar
					onClick={(s) => this.handleFilterChange(s)}
					currentlySortedFilter={this.state.currentlySortedFilter}
				/>
				<div className={`${rootClass}__hotel-entries`}>
					{
						hotels.map(hotel => <HotelEntry hotel={hotel} key={hotel.id} />)
					}
				</div>
			</div>
		);
	}
}

const mapStateToProps = (state) => {
	return {
		searchComplete: state.callSuccess.callSuccess,
		hotels: state.hotels,
	};
};

const mapDispatchToProps = (dispatch) => ({
	sortHotels: bindActionCreators(sortHotels, dispatch)
});

export default connect(mapStateToProps, mapDispatchToProps)(ResultsPage);
