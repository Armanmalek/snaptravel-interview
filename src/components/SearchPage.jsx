import React from 'react';
import SearchFields from './SearchFields.jsx';
import './SearchPage.css';

const rootClass = 'search-page';

export const SearchPage = () => {
	return (
		<div className={rootClass}>
			<SearchFields />
		</div>

	);
};

export default SearchPage;
