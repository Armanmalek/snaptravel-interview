import React from 'react';
import PropTypes from 'prop-types';
import { connect } from 'react-redux';
import './SnapTravelLayout.css';

export const rootClass = 'snap-travel-layout';
const headerClass = `${rootClass}__header`;
const contentClass = `${rootClass}__content`;

export const SnapTravelLayout = ({ children, hasError }) => {
	if (hasError) return <div className={rootClass}>ERROR</div>

	return (
		<div className={rootClass}>
			<div className={headerClass}>
				<img
					className={`${headerClass}__image`}
					src={process.env.PUBLIC_URL + '/images/logo.png'}
					alt="snap travel logo"
				/>
			</div>
			<div className={contentClass}>
				{children}
			</div>
		</div>
	)
};

SnapTravelLayout.propTypes = {
	children: PropTypes.oneOfType([
		PropTypes.arrayOf(PropTypes.node),
		PropTypes.node
	]).isRequired,
	hasError: PropTypes.bool.isRequired,
};

const mapStateToProps = (state) => ({
	hasError: state.hotels.callFailure,
});

export default connect(mapStateToProps, null)(SnapTravelLayout);
