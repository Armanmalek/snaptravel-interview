import { createAction } from 'redux-actions';

export const searchSuccess = createAction('SEARCH_SUCCESS');
export const searchFailure = createAction('SEARCH_FAILURE');
export const sortHotels = createAction('SORT_HOTELS');
