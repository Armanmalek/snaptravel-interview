import * as HotelModels from '../models/hotelModels.js';

const priceSorter = (state) => (
	state.sort((a, b) => {
		if (a.price < b.price) {
			return -1;
		} else if (a.price > b.price) {
			return 1;
		} else {
			return 0;
		}
	})
);

export const searchSuccessParser = (state, payload) => {
	const immutableHotels = payload.map(hotel => HotelModels.HotelModel(hotel));
	return priceSorter(state.push(...immutableHotels));
};

export const hotelsSorter = (state, payload) => {
	if (payload === 'price') return priceSorter(state);

	return state.sort((a, b) => {
		if (a[payload] < b[payload]) {
			return 1;
		} else if (a[payload] > b[payload]) {
			return -1
		} else {
			return 0;
		}
	})
};
