import { handleActions } from 'redux-actions';
import * as HotelActions from '../actions/hotelActions.js';
import { callInitialState } from '../models/callModels.js';

export const callsReducer = handleActions({
	[HotelActions.searchSuccess]: (state) => {
		return state.merge({
			callSuccess: true,
		});
	},

	[HotelActions.searchFailure] : (state) => {
		return state.merge({
			callSuccess: false,
		});
	},
},
	callInitialState
);

export default callsReducer;
