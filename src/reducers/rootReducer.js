import { combineReducers } from 'redux';
import hotelsReducer from './hotelsReducer.js';
import callReducer from './callReducer.js'

export default combineReducers({
	hotels: hotelsReducer,
	callSuccess: callReducer,
});