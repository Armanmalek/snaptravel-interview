import { handleActions } from 'redux-actions';
import { InitialHotelsModel } from '../models/hotelModels.js';
import * as HotelActions from '../actions/hotelActions.js';
import { searchSuccessParser, hotelsSorter } from '../parsers/hotelParsers.js';

export const hotelsReducer = handleActions({
	[HotelActions.searchSuccess]: (state, action) => {
		return searchSuccessParser(state, action.payload);
	},

	[HotelActions.sortHotels]: (state, action) => {
		return hotelsSorter(state, action.payload);
	},
},
	InitialHotelsModel
);

export default hotelsReducer;
