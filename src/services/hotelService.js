import axios from 'axios';
import * as HotelActions from '../actions/hotelActions.js';
import { hotelDuplicateFinder } from '../utils/hotelUtils';

const endpoint = 'https://experimentation.getsnaptravel.com/interview/hotels';

export const callHotels = ({ city, checkIn, checkOut, }) => {
	return (dispatch) => {
		const hotelCall = axios.post(endpoint, {
			city,
			checkin: checkIn,
			checkout: checkOut,
			provider: 'snaptravel',
		});

		const retailCall = axios.post(endpoint, {
			city,
			checkin: checkIn,
			checkout: checkOut,
			provider: 'retail',
		});

		Promise.all([hotelCall, retailCall])
			.then((res) => {
				const matchingHotels = hotelDuplicateFinder(res);
				dispatch(HotelActions.searchSuccess(matchingHotels));
			})
			.catch((e) => {
				console.log('error in search call');
				dispatch(HotelActions.searchFailure());
			});
	};
};
