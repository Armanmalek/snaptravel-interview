import axios from 'axios';
import cookies from 'browser-cookies';
import { LANGUAGE_COOKIE_KEY } from 'store/settings/settingsConstants.js';

const cancelRequest = {};

class RequestBuilder {
	constructor(url, isMarketPlaceApi) {
		// properties
		this.proxyPath = isMarketPlaceApi ? '/api/mkt' : '/api';
		this.url = `${APIServer}${this.proxyPath}${url}`;
		this.method = 'get';
		this.queryParams = [];
		this.data = {};
		this.timeout = 20000;
		this.responseType = 'json';
		this.requestHeaders = {}; // request-specific headers
		this.cancelKey = `${url}`;

		this.language = cookies.get(LANGUAGE_COOKIE_KEY);

		this.successHandler = () => {};
		this.errorHandler = () => {};
	}

	withMethod(method) {
		this.method = method;
		return this;
	}

	withQueryParam(key, value) {
		this.queryParams.push([key, value]);
		return this;
	}

	// allow either a single object, or an array of objects
	// objects in format: { key: '', value: '' }
	withRequestHeader(headers) {
		if (!headers || typeof headers !== 'object') return this;

		// make sure we're working with an array
		const headersArray = (headers instanceof Array) ? headers : [headers];

		// for each array value
		headersArray.forEach((header) => {
			const { key, value } = header;
			if (key && value) {
				this.requestHeaders[key] = value;
			}
		});

		return this;
	}

	withData(data) {
		this.data = data;
		return this;
	}

	withSuccessHandler(callback) {
		this.successHandler = callback;
		return this;
	}

	withErrorHandler(callback) {
		this.errorHandler = callback;
		return this;
	}

	_getUrlParams() {
		return this.queryParams
			.filter(queryParam => queryParam[1])
			.map(queryParam => queryParam.join('=')).join('&');
	}

	_getDefaultHeaders() {
		const site = process.env.SITE;
		const headers = {};

		// language
		if (this.language) {
			headers['Accept-Language'] = this.language;
		}

		// banner
		headers['Site-Banner'] = site;

		return headers;
	}

	_getRequestHeaders() {
		return this.requestHeaders;
	}

	_clearRequestHeaders() {
		this.requestHeaders = {};
	}

	execute() {
		const CancelToken = axios.CancelToken;
		const urlParams = this._getUrlParams();
		const url = `${this.url}${urlParams ? `?${urlParams}` : ''}`;

		// get persistent headers & request headers
		const headers = Object.assign(
			{},
			this._getDefaultHeaders(),
			this._getRequestHeaders()
		);

		// clear request headers
		this._clearRequestHeaders();

		const request = {
			url,
			headers,
			timeout: this.timeout,
			method: this.method,
			data: this.data,
			responseType: this.responseType,
			cancelToken: new CancelToken((c) => {
				cancelRequest[`${this.cancelKey}`] = c;
			}),
		};

		return axios(request).then(this.successHandler).catch(this.errorHandler);
	}
}

export {
	cancelRequest,
};

export default RequestBuilder;
