export const hotelDuplicateFinder = (response) => {
	const retailHotels = response[1].data.hotels.reduce((acc, entry) => {
		acc[entry.id] = entry.price;
		return acc;
	},
		{},
	);

	return response[0].data.hotels.reduce((accumulator, entry) => {
		if (entry.id in retailHotels) {
			const retailPrice = retailHotels[entry.id];

			return [
				...accumulator,
				{
					...entry,
					hotelName: entry.hotel_name,
					rating: entry.num_stars,
					reviews: entry.num_reviews,
					imageUrl: entry.image_url,
					retailPrice,
					savings: retailPrice - entry.price,
				},
			];
		}
		return accumulator;
	},
		[],
	);
};