export const modifyClassName = (className, modifier) => {
	if (!modifier) return className;

	return `${className} ${className}--${modifier}`;
};
